﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    public Nodo[] path;
    public float rango;
    private int actual;

    public Nodo inicio, fin;
    private List<Nodo> resultado;
    // Start is called before the first frame update
    void Start()
    {
        actual = 0;
        StartCoroutine(ChecarSiLlegue());

    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(path[actual].transform.position);
        transform.Translate(transform.forward * Time.deltaTime * 5, Space.World);
        if (Input.GetMouseButtonUp(0)){
            Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(rayo, out hit))
            {
                if (hit.transform.name.Equals("Personaje"))
                {
                    print("Encontrar camino");
                    int comienzo = actual;
                    comienzo--;
                    comienzo %= path.Length;
                    inicio = path[comienzo];
                    float distM=1000000,dist;
                    for (int i = 0; i < path.Length; i++)
                    {
                        dist = Vector3.Distance(transform.position, path[i].transform.position);
                        if (dist < distM && path[i] != inicio)
                        {
                            distM = dist;
                            fin = path[i];
                        }
                    }

                    // Mover a nodo mas cercano
                    resultado = PathFinding.BreadthWise(inicio, fin);
                    print("Inicio " + inicio);
                    print("fin " + fin);
                    StopCoroutine(ChecarSiLlegue());
                    StartCoroutine(IrNodoCercano());

                }
            }
        }
    }

    IEnumerator ChecarSiLlegue()
    {
        float d;
        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            d = Vector3.Distance(transform.position, path[actual].transform.position);
            if (d < rango)
            {
                actual++;
                actual %= path.Length;
            }
        }
    }
    IEnumerator IrNodoCercano()
    {
        float d;
        while (resultado[0] != inicio)
        {
            resultado.RemoveAt(0);
        }

        resultado.RemoveAt(0);
        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            d = Vector3.Distance(transform.position, resultado[0].transform.position);
            if (d < rango)
            {
                resultado.RemoveAt(0);
                if (resultado.Count<=0)
                {
                    inicio = null;
                    fin = null;
                    StopCoroutine(IrNodoCercano());
                    StartCoroutine(ChecarSiLlegue());
                }
            }
        }
    }
}
