﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nodo : MonoBehaviour
{
    public Nodo[] vecinos;
    public List<Nodo> historial;
    public Personaje per;
    // Start is called before the first frame update
    void Start()
    {
        per = FindObjectOfType<Personaje>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        for (int i = 0; i < vecinos.Length; i++)
        {
            Gizmos.DrawLine(transform.position, vecinos[i].transform.position);
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 1);
        if (per.inicio != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(per.inicio.transform.position, 1);
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(per.fin.transform.position, 1);
        }
        else
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(transform.position, 1);
        }
        
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(transform.position, 1);
    }
}
