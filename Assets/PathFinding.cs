﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding // no hereda de monobehaviour
{
    public static List<Nodo> BreadthWise(Nodo inicio, Nodo fin)
    {
        Queue<Nodo> fila = new Queue<Nodo>();
        List<Nodo> visitado = new List<Nodo>();

        inicio.historial = new List<Nodo>();
        fila.Enqueue(inicio);
        visitado.Add(inicio);

        while (fila.Count > 0)
        {
            Nodo actual = fila.Dequeue();
            if (actual == fin)
            {
                // llegamos al resultado
                List<Nodo> resultado = actual.historial;
                resultado.Add(actual);
                return resultado;
            }
            else
            {
                for (int i = 0; i < actual.vecinos.Length; i++)
                {
                    Nodo vecinoActual = actual.vecinos[i];

                    if (!visitado.Contains(vecinoActual))
                    {
                        visitado.Add(vecinoActual);
                        fila.Enqueue(vecinoActual);

                        // agregar historial
                        vecinoActual.historial = new List<Nodo>(actual.historial);
                        vecinoActual.historial.Add(actual);
                    }

                }
            }
        }

        return null;
    }

    public static List<Nodo> Depthwise(Nodo inicio, Nodo fin)
    {
        Stack<Nodo> pila = new Stack<Nodo>();
        List<Nodo> visitado = new List<Nodo>();

        inicio.historial = new List<Nodo>();
        pila.Push(inicio);
        visitado.Add(inicio);

        while (pila.Count > 0)
        {
            Nodo actual = pila.Pop();
            if (actual == fin)
            {
                // llegamos al resultado
                List<Nodo> resultado = actual.historial;
                resultado.Add(actual);
                return resultado;
            }
            else
            {
                for (int i = 0; i < actual.vecinos.Length; i++)
                {
                    Nodo vecinoActual = actual.vecinos[i];

                    if (!visitado.Contains(vecinoActual))
                    {
                        visitado.Add(vecinoActual);
                        pila.Push(vecinoActual);

                        // agregar historial
                        vecinoActual.historial = new List<Nodo>(actual.historial);
                        vecinoActual.historial.Add(actual);
                    }

                }
            }
        }

        return null;
    }
}
